//
//  Message.swift
//  EmergencyBroadcast
//
//  Created by Elliot Yong on 2018-03-19.
//  Copyright © 2018 Elliot Yong. All rights reserved.
//

import Foundation

struct Message: Decodable {
    var from_station: String
    var message: String
    
    enum codingKeys: String, CodingKey {
        case from_station = "from_station"
        case message = "message"
    }
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: codingKeys.self)
        self.from_station = try values.decode(String.self, forKey: .from_station)
        self.message = try values.decode(String.self, forKey: .message)
    }
    
    init(from_station: String, message: String) {
        self.from_station = from_station
        self.message = message
    }
}
