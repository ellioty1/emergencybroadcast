//
//  ViewController.swift
//  EmergencyBroadcast
//
//  Created by Elliot Yong on 2018-03-15.
//  Copyright © 2018 Elliot Yong. All rights reserved.
//

import UIKit
import MapKit
import FirebaseDatabase

class EmergencyViewController: UIViewController {

    @IBOutlet weak var helicopterButton: UIButton!
    @IBOutlet weak var policeButton: UIButton!
    @IBOutlet weak var fireTruckButton: UIButton!
    @IBOutlet weak var ambulanceButton: UIButton!
    @IBOutlet weak var srStationDistance: UILabel!
    @IBOutlet weak var policeStationDistance: UILabel!
    @IBOutlet weak var ambulanceStationDisctance: UILabel!
    @IBOutlet weak var fireHallDistance: UILabel!
    
    let locationManager = CLLocationManager()
    let ambulance = "Ambulance "
    let fireHall = "Fire Hall"
    let police = "Police Station"
    let sR = "Search and Rescue"
    let distanceSuffix = "km away"
    var pointsOfInterest: [String] = [String]()
    var closestStations = [String: CLLocation]()
    var buttonTag = -1
    
    //MARK: Setup
    override func viewDidLoad() {
        super.viewDidLoad()
        locManagerSetUp()
        pointsOfInterest = [ambulance, fireHall, police, sR]
        for poi in pointsOfInterest {
            readDB(station: poi)
        }
    }
    
    func locManagerSetUp() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()
    }
    
    //MARK: Distance Logic
    func readDB(station: String) {
        let dbRef = Database.database().reference().child(station)
        var allStations = [CLLocation]()
        dbRef.observeSingleEvent(of: .value) { (snapshot) in
            var tempStations = [String]()
            for location in snapshot.children.allObjects as! [DataSnapshot] {
                tempStations.append(location.value as! String)
            }
            allStations = self.convertToCoordinates(loStations: tempStations)
            let closestLocation = self.findClosestStation(listofStations: allStations)
            if let closestLoc = closestLocation {
                self.adjustDistanceLabels(station: station, location: closestLoc)
            }
        }
    }
    
    func writeDB() {
        let userLocation = convertLocationToString()
        let dbRef = Database.database().reference().child("Locations")
        dbRef.childByAutoId().setValue(userLocation)
    }
    
    func adjustDistanceLabels(station: String, location: CLLocation) {
        guard let userLocation = locationManager.location else { return }
        var distance: String = ""
        if station == ambulance {
            distance = calculateDistance(userLocation: userLocation, stationLocation: location)
            ambulanceStationDisctance.text = distance
        } else if station == fireHall {
            distance = calculateDistance(userLocation: userLocation, stationLocation: location)
            fireHallDistance.text = distance
        } else if station == police {
            distance = calculateDistance(userLocation: userLocation, stationLocation: location)
            print(location)
            policeStationDistance.text = distance
        } else if station == sR {
            distance = calculateDistance(userLocation: userLocation, stationLocation: location)
            srStationDistance.text = distance
        }
    }
    
    func calculateDistance(userLocation: CLLocation, stationLocation: CLLocation) -> String {
        let distance = userLocation.distance(from: stationLocation) / 1000
        return String(distance.rounded(toPlaces: 2)) + distanceSuffix
    }
    
    func convertToCoordinates(loStations: [String]) -> [CLLocation] {
        var clStations = [CLLocation]()
        for station in loStations {
            let lat = station.components(separatedBy: ",")[0]
            let long = station.components(separatedBy: ",")[1]
            let latitude =  (lat as NSString).doubleValue
            let longitude = (long as NSString).doubleValue
            let coordinate = CLLocation(latitude: latitude, longitude: longitude)
            clStations.append(coordinate)
        }
        return clStations
    }
    
    func convertLocationToString() -> String? {
        guard let userLocation = locationManager.location else { return nil }
        let latLong = String(describing: userLocation.coordinate.latitude) + " " + String(describing: userLocation.coordinate.longitude)
        return latLong
    }
    
    func findClosestStation(listofStations: [CLLocation]) -> CLLocation? {
        guard let userLocation = locationManager.location else { return nil }
        var closestLocation: CLLocation = CLLocation(latitude: 64.7511, longitude: 147.3494)
        var tempLocation: CLLocation
        for station in listofStations {
            tempLocation = station
            if userLocation.distance(from: tempLocation) < userLocation.distance(from: closestLocation) {
                closestLocation = tempLocation
            }
        }
        return closestLocation
    }
    
    //MARK: IBActions and Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toChatVC" {
            if let chatVC = segue.destination as? ChatViewController {
                chatVC.userLocation = locationManager.location!
                switch buttonTag  {
                case 0:
                    chatVC.emergencyType = "Ambulance"
                case 1:
                    chatVC.emergencyType = "Fire Truck"
                case 2:
                    chatVC.emergencyType = "Police"
                case 3:
                    chatVC.emergencyType = "Rescue Team"
                default:
                    print("Nothing was selected")
                }
            }
        }
    }
    
    @IBAction func phone911(_ sender: Any) {
        if let url = URL(string: "tel://1234567890"), UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.open(url)
        }
    }
    @IBAction func ambulanceAction(_ sender: Any) {
        buttonTag = 0
        writeDB()
        performSegue(withIdentifier: "toChatVC", sender: self)
    }
    
    @IBAction func firetruckAction(_ sender: Any) {
        buttonTag = 1
        writeDB()
        performSegue(withIdentifier: "toChatVC", sender: self)
    }
    
    @IBAction func policeAction(_ sender: Any) {
        buttonTag = 2
        writeDB()
        performSegue(withIdentifier: "toChatVC", sender: self)
    }
    
    @IBAction func helicopterAction(_ sender: Any) {
        buttonTag = 3
        writeDB()
        performSegue(withIdentifier: "toChatVC", sender: self)
    }

}

extension EmergencyViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if locations.first != nil {
            print("location:: (location)")
        }
        
    }
}

extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

