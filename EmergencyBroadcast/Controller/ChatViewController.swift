//
//  ChatViewController.swift
//  EmergencyBroadcast
//
//  Created by Elliot Yong on 2018-03-17.
//  Copyright © 2018 Elliot Yong. All rights reserved.
//

import UIKit
import MapKit

class ChatViewController: UIViewController {

    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var msgBarStack: UIStackView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var chatCollectionView: UICollectionView!
    
    
    let police = "Police"
    let ambulance = "Ambulance"
    let sR = "Rescue Team"
    let firetruck = "Fire Truck"
    let widthOffset: CGFloat = 30.0
    var chatRadius: CGFloat = 10.0
    var userLocation: CLLocation!
    var messages: [Message] = [Message]()
    var emergencyType: String = ""
    var operatorMessage: Message = Message(from_station: "ambulance", message: "This is a sample message from the operator.")
    
    //MARK: Setup and keyboard
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
        addKeyboardObservers()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    func addKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            bottomConstraint.constant += keyboardSize.height
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.bottomConstraint.constant != 0{
                print(bottomConstraint.constant)
                bottomConstraint.constant -= keyboardSize.height
                print(bottomConstraint.constant)
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setUpView() {
        self.view.bringSubview(toFront: msgBarStack);
        sendButton.isEnabled = false
        messageTextField.delegate = self
        msgBarStack.addBackground(color: UIColor.white)
        UIApplication.shared.statusBarStyle = .lightContent
        let latLong = String(userLocation.coordinate.latitude) + String(userLocation.coordinate.longitude)
        let initialMessage = Message(from_station: emergencyType, message: "We are sending the \(emergencyType) to: \(latLong)")
        messages.append(initialMessage)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    //MARK: Send Message functionality
    @IBAction func sendButtonAction(_ sender: Any) {
        if let myMsg = messageTextField.text {
            var newMessage: Message = Message(from_station: "User", message: myMsg)
            newMessage.from_station = "User"
            newMessage.message = myMsg
            messages.append(newMessage)
            insertNewChatMessage()
            postToOperator(message: myMsg)
            messageTextField.text = nil
        }
    }
    
    func insertNewChatMessage() {
        let item = (messages.count) - 1
        let insertIndexPath = IndexPath(row: item, section: 0)
        chatCollectionView.insertItems(at: [insertIndexPath])
        chatCollectionView.scrollToItem(at: insertIndexPath, at: .bottom, animated: true)
    }
    
    func postToOperator(message: String) {
        operatorMessage.from_station = emergencyType
        messages.append(self.operatorMessage)
        insertNewChatMessage()
        chatCollectionView.reloadData()
    }
}

extension ChatViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return messages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let frame: CGRect = estimatedFrame(text: messages[indexPath.row].message)
        if messages[indexPath.row].from_station != "User" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  "emergencyCell", for: indexPath) as! EmergencyServiceCell
            cell.emergencyText.text = messages[indexPath.row].message
            cell.emergencyText.layer.cornerRadius = chatRadius
            cell.textWidth.constant = frame.width + widthOffset
            if messages[indexPath.row].from_station == ambulance {
                cell.emergencyImage.image = UIImage(named: "ambulance")
                return cell
            } else if messages[indexPath.row].from_station == firetruck {
                cell.emergencyImage.image = UIImage(named: "fireTruck")
                return cell
            } else if messages[indexPath.row].from_station == police {
                cell.emergencyImage.image = UIImage(named: "police")
                return cell
            } else if messages[indexPath.row].from_station == sR {
                cell.emergencyImage.image = UIImage(named: "helicopter")
                return cell
            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as! UserMessageCell
            cell.usesrTextField.text = messages[indexPath.row].message
            cell.usesrTextField.layer.cornerRadius = chatRadius
            cell.textFieldWidth.constant = frame.width + 30.0
            return cell
        }
        
        let defaultCell: UICollectionViewCell = UICollectionViewCell()
        return defaultCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var height: CGFloat = 100
        let messageText = messages[indexPath.row].message
        height = estimatedFrame(text: messageText).height
        return CGSize(width: view.frame.width, height: height + 15)
    }
    
    private func estimatedFrame(text: String) -> CGRect {
        let size = CGSize(width: 200, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
    }
    

}

extension ChatViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField.hasText {
            sendButton.isEnabled = true
        } else {
            sendButton.isEnabled = false
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !textField.hasText {
            sendButton.isEnabled = false
        } else {
            sendButton.isEnabled = true
        }
        return true
    }
}

extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
