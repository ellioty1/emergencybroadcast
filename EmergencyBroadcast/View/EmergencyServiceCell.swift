//
//  EmergencyServiceCell.swift
//  EmergencyBroadcast
//
//  Created by Elliot Yong on 2018-03-19.
//  Copyright © 2018 Elliot Yong. All rights reserved.
//

import UIKit

class EmergencyServiceCell: UICollectionViewCell {
    
    @IBOutlet weak var textWidth: NSLayoutConstraint!
    @IBOutlet weak var emergencyImage: UIImageView!
    @IBOutlet weak var emergencyText: UILabel!
}
