//
//  RoundedButton.swift
//  EmergencyBroadcast
//
//  Created by Elliot Yong on 2018-03-16.
//  Copyright © 2018 Elliot Yong. All rights reserved.
//

import UIKit


class RoundedButton: UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    private func setup() {
        self.layer.cornerRadius = 40
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.masksToBounds = true
    }
}
