# EmergencyBroadcast

The app takes your location and finds the closest emergency services to you. You then have the option of deciding which service you need. Once you decide, your location will be written to the database for the operator to see. You then have the option of chatting with operator. With this current implementation, there is no "Operator", so the chat is hardcoded.

I did not use very many libraries as I wanted to show my coding ability as best I could.

***Before running on the simulator, make sure the hardware keyboard is enabled.***
